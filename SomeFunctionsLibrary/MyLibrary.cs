﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SomeFunctionsLibrary
{
    public class MyLibrary
    {

        /// <summary>
        /// Вычисляет площадь прямоугольного треугольника, заданного длинами его сторон
        /// </summary>
        /// <param name="FirstSideLength">Длина первой стороны треугольника</param>
        /// <param name="SecondSideLength">Длина второй стороны треугольника</param>
        /// <param name="ThirthSideLength">Длина третьей стороны треугольника</param>
        /// <returns>Площадь треугольника</returns>
        /// <exception cref="System.ArgumentException">Выбрасывается, если одна из длин сторон задана отрицательной или если треугольник не является прямоугольным</exception>
        public static double GetRightTriangleSquare(double FirstSideLength, double SecondSideLength, double ThirthSideLength)
        {
            if (FirstSideLength < 0)
                throw new ArgumentException("Отрицательная длина стороны", "FirstSideLength");
            if (SecondSideLength < 0)
                throw new ArgumentException("Отрицательная длина стороны", "SecondSideLength");
            if (ThirthSideLength < 0)
                throw new ArgumentException("Отрицательная длина стороны", "ThirthSideLength");
            if ((FirstSideLength + SecondSideLength < ThirthSideLength) || (FirstSideLength + ThirthSideLength < SecondSideLength) || (SecondSideLength + ThirthSideLength < FirstSideLength))
                throw new ArgumentException("Длины сторон заданы неверно - не выполнено неравенство треугольника");

            if (Math.Min(FirstSideLength, Math.Min(SecondSideLength, ThirthSideLength)) == 0) // вырожденный случай - отрезок
                return 0;

            double MaxLength = Math.Max(FirstSideLength, Math.Max(SecondSideLength, ThirthSideLength)); // находим максимальную из длин сторон (длину гипотенузы)
            if (MaxLength == FirstSideLength) // гипотенуза - первая сторона
            {
                if (IsRightTriangle(FirstSideLength, SecondSideLength, ThirthSideLength))
                    return SecondSideLength * ThirthSideLength / 2;
                else
                    throw new ArgumentException("Треугольник не является прямоугольным");
            }
            else if (MaxLength == SecondSideLength) // гипотенуза - вторая сторона
            {
                if (IsRightTriangle(SecondSideLength, FirstSideLength, ThirthSideLength))
                    return FirstSideLength * ThirthSideLength / 2;
                else
                    throw new ArgumentException("Треугольник не является прямоугольным");
            }
            else // гипотенуза - третья сторона
            {
                if (IsRightTriangle(ThirthSideLength, FirstSideLength, SecondSideLength))
                    return FirstSideLength * SecondSideLength / 2;
                else
                    throw new ArgumentException("Треугольник не является прямоугольным");
            }
        }

        /// <summary>
        /// Проверяет, является ли треугольник с заданными длинами сторон прямоугольным. Аргументы должны быть положительными
        /// </summary>
        private static bool IsRightTriangle(double Hypotenuse, double Leg1, double Leg2)
        {
            double cosA = (Leg1 * Leg1 + Leg2 * Leg2 - Hypotenuse * Hypotenuse) / 2 / Leg1 / Leg2;
            return (Math.Abs(cosA) < 0.01); // небольшое отклонение от прямого угла считаем нестрашным
        }

    }
}

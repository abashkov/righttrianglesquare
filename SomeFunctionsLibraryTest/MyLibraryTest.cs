﻿using SomeFunctionsLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace SomeFunctionsLibraryTest
{
    
    
    /// <summary>
    ///Это класс теста для MyLibraryTest, в котором должны
    ///находиться все модульные тесты MyLibraryTest
    ///</summary>
    [TestClass()]
    public class MyLibraryTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты теста
        // 
        //При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        //ClassInitialize используется для выполнения кода до запуска первого теста в классе
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //TestInitialize используется для выполнения кода перед запуском каждого теста
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //TestCleanup используется для выполнения кода после завершения каждого теста
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///Тест для GetRightTriangleSquare
        ///</summary>
        [TestMethod()]
        public void GetRightTriangleSquareTest()
        {
            // треугольник с нулевой стороной
            double FirstSideLength = 0;
            double SecondSideLength = 4;
            double ThirthSideLength = 4;
            double expected = 0;
            double actual = MyLibrary.GetRightTriangleSquare(FirstSideLength, SecondSideLength, ThirthSideLength);
            Assert.AreEqual(expected, actual);

            // классический египетский прямоугольный треугольник
            FirstSideLength = 3;
            SecondSideLength = 5;
            ThirthSideLength = 4;
            expected = 6;
            actual = MyLibrary.GetRightTriangleSquare(FirstSideLength, SecondSideLength, ThirthSideLength);
            Assert.AreEqual(expected, actual);

            // тоже известный прямоугольный треугольник
            FirstSideLength = 5;
            SecondSideLength = 12;
            ThirthSideLength = 13;
            expected = 30;
            actual = MyLibrary.GetRightTriangleSquare(FirstSideLength, SecondSideLength, ThirthSideLength);
            Assert.AreEqual(expected, actual);

            // египетский прямоугольный треугольник с одной из сторон, заданной с погрешностью
            FirstSideLength = 5;
            SecondSideLength = 3;
            ThirthSideLength = 3.998;
            expected = 5.997;
            actual = MyLibrary.GetRightTriangleSquare(FirstSideLength, SecondSideLength, ThirthSideLength);
            Assert.AreEqual(expected, actual, 100 * Double.Epsilon);

            // равнобедренный прямоугольный треугольник с единичным катетом и с гипотенузой, заданной с погрешностью
            FirstSideLength = 1.0;
            SecondSideLength = 1.41; // sqrt(2)
            ThirthSideLength = 1;
            expected = 0.5;
            actual = MyLibrary.GetRightTriangleSquare(FirstSideLength, SecondSideLength, ThirthSideLength);
            Assert.AreEqual(expected, actual);

            // некий прямоугольный треугольник со сторонами, округлёнными до двух знаков после запятой
            FirstSideLength = 12.34;
            SecondSideLength = 47.69;
            ThirthSideLength = 46.07;
            expected = 284.2519;
            actual = MyLibrary.GetRightTriangleSquare(FirstSideLength, SecondSideLength, ThirthSideLength);
            Assert.AreEqual(expected, actual, 100 * Double.Epsilon);
        }

        /// <summary>
        ///Тест на выброс исключения ArgumentException для GetRightTriangleSquare
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentException))]
        public void GetRightTriangleSquareTest_WithArgumentException()
        {
            double FirstSideLength;
            double SecondSideLength;
            double ThirthSideLength;
            double actual;

            FirstSideLength = 1.1;
            SecondSideLength = 0;
            ThirthSideLength = -5.07;
            actual = MyLibrary.GetRightTriangleSquare(FirstSideLength, SecondSideLength, ThirthSideLength); // тест на отрицательную длину стороны

            FirstSideLength = 4;
            SecondSideLength = -5;
            ThirthSideLength = -3.002;
            actual = MyLibrary.GetRightTriangleSquare(FirstSideLength, SecondSideLength, ThirthSideLength); // тест на отрицательную длину стороны

            FirstSideLength = 1.1;
            SecondSideLength = 2.22;
            ThirthSideLength = 5.64;
            actual = MyLibrary.GetRightTriangleSquare(FirstSideLength, SecondSideLength, ThirthSideLength); // тест на невыполнение нравенства треугольника

            FirstSideLength = 11.1;
            SecondSideLength = 5;
            ThirthSideLength = 0;
            actual = MyLibrary.GetRightTriangleSquare(FirstSideLength, SecondSideLength, ThirthSideLength); // тест на невыполнение нравенства треугольника

            FirstSideLength = 3.00;
            SecondSideLength = 4;
            ThirthSideLength = 6.0;
            actual = MyLibrary.GetRightTriangleSquare(FirstSideLength, SecondSideLength, ThirthSideLength); // тест на непрямоугольность треугольника

            FirstSideLength = 1;
            SecondSideLength = 1.3;
            ThirthSideLength = 1;
            actual = MyLibrary.GetRightTriangleSquare(FirstSideLength, SecondSideLength, ThirthSideLength); // тест на непрямоугольность треугольника
        }

    }
}
